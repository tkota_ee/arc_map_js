require([
    "esri/views/SceneView", 
    "esri/portal/PortalItem", 
    "esri/WebScene",
    "dojo/domReady!" 
    ], function( 
        SceneView,
        PortalItem,
        WebScene
    ) { 
        var scene = new WebScene({
        portalItem: new PortalItem({
        id: "9c7a9ad386954c7782d621071027e148" 
        })
    
    });
    var view = new SceneView({
        map: scene,
        container: "map1" 
    });
    }); 