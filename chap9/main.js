require([
    "esri/Map",
    "esri/views/SceneView",
    "esri/widgets/Search",
    "esri/widgets/Search/SearchViewModel",
    "esri/widgets/BasemapToggle",
    "esri/widgets/BasemapToggle/BasemapToggleViewModel",
    "dojo/domReady!"
], function (
    Map,
    SceneView,
    Search,
    SearchVM, BasemapToggle, BasemapToggleVM) {
    var map = new Map({
        basemap: "topo"
    });
    var view = new SceneView({
        scale: 150000000,
        container: "map1",
        map: map
    });
    var searchWgt = new Search({
        viewModel: new SearchVM({
            view: view
        })
    }, "searchDiv");
    
    var toggleWgt = new BasemapToggle({
        viewModel: new BasemapToggleVM({
            view: view,
            secondaryBasemap: "streets",
        })
    }, "toggleDiv");
    
    toggleWgt.startup();
    searchWgt.startup();


});