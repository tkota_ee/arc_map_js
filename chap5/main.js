require(["esri/map", "dojo/domReady!"], function (Map) {
    //add code here to add a map 
});

require([
    "esri/Map", "esri/views/SceneView", "dojo/domReady!"
], function (Map, SceneView) {
    var map = new Map({
        basemap: "topo",
    });
    var view = new SceneView({
        container: "map1",
        map: map,
        camera: {
            position: [-122.4479, 37.7531, 10000],
            tilt: 50,
            heading: 330
        }
        // scale: 1000000,
        // center: [-122.4479, 37.7531,100]
    });
    

});
