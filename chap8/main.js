require(["esri/map", "dojo/domReady!"], function (Map) {
    
});

require([
            "esri/Map",
            "esri/views/SceneView",
            "esri/widgets/Search",
            "esri/widgets/Search/SearchViewModel",
            "dojo/domReady!"
        ], function (
            Map, SceneView, Search, SearchVM) {
            var map = new Map({
                basemap: "topo"
            });
            var view = new SceneView({
                scale: 150000000,
                container: "map1",
                map: map
            });
            var searchWgt = new Search({
                viewModel: new SearchVM({
                    view: view
                })
            }, "searchDiv");
            searchWgt.startup();
});








